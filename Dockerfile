FROM debian:latest as multi_arch
ARG TARGETARCH

# Ensure that apt install does not prompt interactively
ENV DEBIAN_FRONTEND noninteractive

# Disable auto installation of recommended packages, too many unwanted packages gets installed without this
RUN apt-config dump | grep -we Recommends -e Suggests | sed s/1/0/ | tee /etc/apt/apt.conf.d/999norecommend


# Required packages
RUN apt update && \
    apt install -y \
    ca-certificates \
    curl \
    openssh-server \
    supervisor \
    && apt clean autoclean && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*deb

# Add more repositories
# MS does not package powershell for Debian >11 at this time
RUN curl -fLO https://packages.microsoft.com/config/debian/11/packages-microsoft-prod.deb && dpkg -i packages-microsoft-prod.deb && rm packages-microsoft-prod.deb
#RUN curl -fLO https://packages.microsoft.com/config/debian/$(grep '^VERSION_ID=' /etc/os-release | awk -F '\"' '{print $2}')/packages-microsoft-prod.deb && dpkg -i packages-microsoft-prod.deb && rm packages-microsoft-prod.deb

# Utils
RUN apt update && apt install -y \
    bash-completion \
    bc \
    bind9-dnsutils \
    binutils \
    bridge-utils \
    dnsmasq \
    dos2unix \
    fakeroot \
    file \
    fuse \
    gdb \
    git \
    gnupg \
    ipmitool \
    iproute2 \
    iputils-ping \
    j2cli \
    jq \
    less \
    man-db \
    manpages \
    manpages-dev \
    moreutils \
    net-tools \
    netcat-traditional \
    nginx \
    nmap \
    mtr-tiny \
    openssh-client \
    openvpn \
    pciutils \
    procps \
    proxychains \
    psmisc \
    python3-pip \
    python3-venv \
    rsync \
    screen \
    snmp \
    socat \
    strace \
    sshpass \
    tcpdump \
    tmux \
    traceroute \
    vim \
    wget \
    xq \
    xz-utils \
    yq \
    zip \
    && apt clean autoclean && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*deb

# QEMU utils
RUN apt update && apt install -y \
      cloud-utils \
      ovmf \
      qemu-efi \
      qemu-system-arm \
      qemu-system-x86 \
      qemu-utils \
      swtpm \
      swtpm-tools \
      && apt clean autoclean && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*deb

# Other Utils
RUN curl -fL https://github.com/cilium/cilium-cli/releases/latest/download/cilium-linux-${TARGETARCH}.tar.gz | tar -xz -C /usr/local/bin cilium && chmod +x /usr/local/bin/cilium && \
  curl -fL https://get.helm.sh/helm-$(curl -fL -s https://get.helm.sh/helm-latest-version)-linux-${TARGETARCH}.tar.gz | tar -xz --strip-components=1 -C /usr/local/bin linux-${TARGETARCH}/helm && chmod +x /usr/local/bin/helm && \
  curl -fLO https://github.com/derailed/k9s/releases/latest/download/k9s_linux_${TARGETARCH}.deb && dpkg -i k9s_linux_${TARGETARCH}.deb && rm k9s_linux_${TARGETARCH}.deb && \
  curl -fo /usr/local/bin/kubectl -L "https://dl.k8s.io/release/$(curl -fL -s https://dl.k8s.io/release/stable.txt)/bin/linux/${TARGETARCH}/kubectl" && chmod +x /usr/local/bin/kubectl && \
  curl -fL https://github.com/kubernetes-sigs/kustomize/releases/latest/download/$(curl -fL -s https://api.github.com/repos/kubernetes-sigs/kustomize/releases/latest | jq -r '.assets[].name' | grep "_linux_${TARGETARCH}.tar.gz\$") | tar -xz -C /usr/local/bin kustomize && chmod +x /usr/local/bin/kustomize && \
  curl -fL https://github.com/oras-project/oras/releases/latest/download/$(curl -fL -s https://api.github.com/repos/oras-project/oras/releases/latest | jq -r '.assets[].name' | grep "_linux_${TARGETARCH}.tar.gz\$") | tar -xz -C /usr/local/bin oras && chmod +x /usr/local/bin/oras && \
  curl -fo /usr/local/bin/skopeo -L https://github.com/lework/skopeo-binary/releases/latest/download/skopeo-linux-${TARGETARCH} && chmod +x /usr/local/bin/skopeo

RUN sed -i'' 's/#PermitRootLogin .*/PermitRootLogin yes/g' /etc/ssh/sshd_config

COPY files/etc/vim/vimrc.local /etc/vim/vimrc.local

COPY files/root/spawn-qemu.sh /root/spawn-qemu.sh

COPY files/etc/supervisor/conf.d/* /etc/supervisor/conf.d/
COPY files/docker-entrypoint /docker-entrypoint


FROM multi_arch as amd64-final

# Utils
RUN apt update && apt install -y \
    powershell \
  && apt clean autoclean && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*deb

# Powershell modules
SHELL ["pwsh", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]
RUN Set-PSRepository -Name PSGallery -InstallationPolicy Trusted; \
    Install-Module -AllowClobber -Force -Name VMware.PowerCLI; Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP:$false -Confirm:$false
SHELL ["/bin/sh", "-c"]

# Other Utils
RUN curl -fo /usr/local/bin/crane -L https://github.com/michaelsauter/crane/releases/latest/download/crane_linux_${TARGETARCH} && chmod +x /usr/local/bin/crane

EXPOSE 22 80

ENTRYPOINT ["/docker-entrypoint"]
CMD [ "/usr/bin/supervisord", "-n" ]


FROM multi_arch as arm64-final

EXPOSE 22 80

ENTRYPOINT ["/docker-entrypoint"]
CMD [ "/usr/bin/supervisord", "-n" ]
