Tricorder is a container filled with various tools for analyzing and debugging the environment that you deploy it into.

#### Deployment and usage in k8s

If you are using a private registry, make sure to create the auth secrets eg.:

~~~
kubectl create secret docker-registry regcred --docker-server=registry.starfleet.subspace --docker-username='spock' --docker-password='l1v3l0ng'
~~~

And add the secret to k8s-deployment.yml:

~~~
kind: Deployment
...
spec:
  ...
  template:
    ...
    spec:
      ...
      imagePullSecrets:
      - name: regcred
~~~

Further modify the deployment to your needs and deploy the thing

~~~
kubectl apply -f k8s-deployment.yml
~~~

Examples of usage (assuming that you have deployed using the above manifest)

~~~
export TRICORDER_ROOT_PASS=$(kubectl logs $(kubectl get pod -l 'run=tricorder' -o='jsonpath'="{.items[*].metadata.name}") --tail=10000 | grep '^root password:' | tail -n1 | awk '{print $NF}')
export TRICORDER_NODE_NAME=$(kubectl get pod -l 'run=tricorder' -o='jsonpath'="{.items[*].spec.nodeName}")
export TRICORDER_NODE_PORT_SSH=$(kubectl get service -l 'run=tricorder' -o='jsonpath'="{.items[*].spec.ports[?(@.name=='ssh')].nodePort}")
export TRICORDER_NODE_PORT_HTTP=$(kubectl get service -l 'run=tricorder' -o='jsonpath'="{.items[*].spec.ports[?(@.name=='http')].nodePort}")
export TRICORDER_CONTAINER_NAME=$(kubectl get pod -l 'run=tricorder' -o='jsonpath'="{.items[*].metadata.name}")
kubectl exec $TRICORDER_CONTAINER_NAME -- ssh-keyscan localhost -f - 2>/dev/null | sed "s/^localhost /[$TRICORDER_NODE_NAME]:$TRICORDER_NODE_PORT_SSH /g" > ~/.tricorder_known_hosts

$ sshpass -p $TRICORDER_ROOT_PASS ssh $TRICORDER_NODE_NAME -l root -p $TRICORDER_NODE_PORT_SSH -o 'UserKnownHostsFile=~/.tricorder_known_hosts'
root@tricorder-65dd99f46d-pw2tw:~#

$ curl -O $TRICORDER_NODE_NAME:$TRICORDER_NODE_PORT_HTTP/usr/local/bin/skopeo
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 36.3M  100 36.3M    0     0  8953k      0  0:00:04  0:00:04 --:--:-- 8957k
$
~~~

A more exotic usecase is running a QEMU machine inside the container

~~~
root@tricorder-65dd99f46d-zt9ml:~# ./spawn-qemu.sh
Below are the provided settings, and also how to override them:
export QEMU_ARCH="amd64"
export QEMU_IMG_FILE="/root/qemu/54503-root.img"
export QEMU_IMG_URL="https://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-amd64.img"
export QEMU_ISO_FILE="/root/qemu/54503-cloud-init.iso"
export QEMU_CONFIG_ID="54503"
export QEMU_SSH_PUBKEY="generate"

/root/qemu/54503-root.img not found - trying to download https://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-amd64.img
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  618M  100  618M    0     0  92.2M      0  0:00:06  0:00:06 --:--:--  101M
QEMU machine is booting ..
  console: screen -RDS qemu-54503
  ssh: ssh -o SendEnv=* -i /root/qemu/ssh_key localhost -p2222
root@tricorder-65dd99f46d-zt9ml:~# ssh -o SendEnv=* -i /root/qemu/ssh_key localhost -p2222
...
root@ubuntu:~# apt update && apt install -y docker.io
...
root@ubuntu:~# docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
c1ec31eb5944: Pull complete
Digest: sha256:d000bc569937abbe195e20322a0bde6b2922d805332fd6d8a68b19f524b7d21d
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
...
~~~

Notice that the container does not carry the disk image for the QEMU machine, it has to be downloaded or carried in by you.

#### Deployment and usage with docker compose

Modify the compose file to your needs and deploy the thing

~~~
docker compose up -d
~~~

Then you can ssh into the same fashion as with the k8s deployment:

~~~
export TRICORDER_ROOT_PASS=$(docker logs tricorder --tail=10000 2>/dev/null | grep '^root password:' | tail -n1 | awk '{print $NF}')
docker exec tricorder ssh-keyscan localhost -f - 2>/dev/null | sed "s/^localhost /[localhost]:2222 /g" > ~/.tricorder_known_hosts

sshpass -p $TRICORDER_ROOT_PASS ssh localhost -l root -p 2222 -o 'UserKnownHostsFile=~/.tricorder_known_hosts'
root@65e1b4312cf1:~#
~~~

But you can also SSH into the container as yourself (the compose file assumes that you have UID 1000 though).

The easiest way to do this is adding this `.ssh/config`:

~~~
Match host localhost exec "test %p = 2222"
  UserKnownHostsFile=~/.tricorder_known_hosts
~~~

And then create a bin like `~/bin/tricorder`:

~~~
docker exec tricorder ssh-keyscan localhost -f - 2>/dev/null | sed "s/^localhost /[localhost]:2222 /g" > ~/.tricorder_known_hosts
[ -f ~/.ssh/authorized_keys ] || sh -c 'cat ~/.ssh/id_*.pub >> ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys'
ssh localhost -p 2222
~~~

As your home directory is mounted in, so is your authorized_keys.

#### Usage as a chroot filesystem

Included is a helper script, that will launch the containers filesystem as a chroot environment instead. It will look for `tricorder-fs-<ARCH>.tar` already being there, enabling you to easily bring it in from somewhere else.

~~~
./chroot.sh 
Did not find tricorder-fs-amd64.tar, trying to create it with docker export
[+] Running 1/0
 ✔ Container tricorder  Running                                                                                                               0.0s 
[+] Running 2/2
 ✔ Container tricorder        Removed                                                                                                         1.0s 
 ✔ Network tricorder_default  Removed                                                                                                         0.3s 
bash: warning: setlocale: LC_ALL: cannot change locale (en_US.UTF-8)
root@somehost:/# tcpdump -ni eth0
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on eth0, link-type EN10MB (Ethernet), snapshot length 262144 bytes
05:25:01.430162 IP 1.2.3.4.57621 > 1.2.3.5.57621: UDP, length 44
...
~~~

#### Deployment inside the GitLab Kubernetes executor

Example of a reverse shell in `.gitlab-ci.yml`

~~~
default:
  image:
    name: registry.kristensen.io/kristensenio/tricorder
  tags:
    - shared

stages:
  - debug_shell

tricorder_shell:
  stage: debug_shell
  script:
    - socat exec:'bash -li',pty,stderr,setsid,sigint,sane tcp:1.2.3.4:4444
  timeout: 5h
~~~

You need to have this running in the other end:

~~~
while true; do socat file:`tty`,raw,echo=0 tcp-listen:4444; sleep 1; done
~~~

Remember to specify the `DOCKER_AUTH_CONFIG` CI/CD variable if pulling from a private registry.
