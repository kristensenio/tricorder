#!/bin/bash

test -d /root/qemu || mkdir /root/qemu

QEMU_ARCH="${QEMU_ARCH:-amd64}"
QEMU_IMG_URL="${QEMU_IMG_URL:-https://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-${QEMU_ARCH}.img}"
QEMU_SSH_PUBKEY="${QEMU_SSH_PUBKEY:-generate}"

QEMU_CONFIG_ID="${QEMU_CONFIG_ID:-TEMPID}"
i=1
while [ $i -le 2 ]; do
  TMP_QEMU_IMG_FILE="${QEMU_IMG_FILE:-/root/qemu/${QEMU_CONFIG_ID}-root.img}"
  TMP_QEMU_ISO_FILE_CLOUDINIT="/root/qemu/${QEMU_CONFIG_ID}-cloud-init.iso"
  TMP_QEMU_ISO_FILE="${QEMU_ISO_FILE:-$TMP_QEMU_ISO_FILE_CLOUDINIT}"

  if [ $i -eq 1 ]; then
    if [ "$QEMU_CONFIG_ID" = "TEMPID" ]; then
      QEMU_CONFIG_ID=$(echo $QEMU_ARCH $QEMU_IMG_URL $QEMU_SSH_PUBKEY $TMP_QEMU_IMG_FILE $TMP_QEMU_ISO_FILE | md5sum | grep -o '^.....')
    fi
  fi
  i=$(( $i + 1 ))
done
QEMU_IMG_FILE="$TMP_QEMU_IMG_FILE"
QEMU_ISO_FILE_CLOUDINIT="$TMP_QEMU_ISO_FILE_CLOUDINIT"
QEMU_ISO_FILE="$TMP_QEMU_ISO_FILE"

echo "Below are the provided settings, and also how to override them:"
echo "export QEMU_ARCH=\"$QEMU_ARCH\""
echo "export QEMU_IMG_FILE=\"$QEMU_IMG_FILE\""
echo "export QEMU_IMG_URL=\"$QEMU_IMG_URL\""
echo "export QEMU_ISO_FILE=\"$QEMU_ISO_FILE\""
echo "export QEMU_CONFIG_ID=\"$QEMU_CONFIG_ID\""
echo "export QEMU_SSH_PUBKEY=\"$QEMU_SSH_PUBKEY\""
echo

if [ "$QEMU_SSH_PUBKEY" = "generate" ]; then
  if [ ! -f /root/qemu/ssh_key ]; then
    ssh-keygen -t ed25519 -f /root/qemu/ssh_key -q -N ""
  fi
  export QEMU_SSH_PUBKEY=$(cat /root/qemu/ssh_key.pub | tr -d '\n')
else
  export QEMU_SSH_PUBKEY
fi

if [ "$QEMU_ISO_FILE" = "$QEMU_ISO_FILE_CLOUDINIT" ]; then

  if [ ! -f /root/qemu/${QEMU_CONFIG_ID}-cloud-init.yml ]; then
    echo "#cloud-config
disable_root: 0
ssh_authorized_keys:
  - $QEMU_SSH_PUBKEY
runcmd:
 - 'chmod -x /etc/update-motd.d/*'
write_files:
  - path: /etc/ssh/sshd_config
    content: |
       AcceptEnv *
    append: true
runcmd:
 - 'systemctl restart sshd'" > /root/qemu/${QEMU_CONFIG_ID}-cloud-init.yml
  fi

  if [ ! -f ${QEMU_ISO_FILE_CLOUDINIT} ]; then
    cloud-localds $QEMU_ISO_FILE_CLOUDINIT /root/qemu/${QEMU_CONFIG_ID}-cloud-init.yml
  fi

else
  if [ ! -f ${QEMU_ISO_FILE} ]; then
    echo "$QEMU_ISO_FILE not found"
    exit 1
  fi
fi

if [ ! -f ${QEMU_IMG_FILE} ]; then
  echo "$QEMU_IMG_FILE not found - trying to download $QEMU_IMG_URL"
  curl -fLo ${QEMU_IMG_FILE} ${QEMU_IMG_URL}
  if [ $? -ne 0 ]; then
    exit 1
  fi
fi

if [ ${QEMU_ARCH} = "amd64" ]; then
  screen -d -S qemu-$QEMU_CONFIG_ID -m qemu-system-x86_64 \
    -cpu max -smp cores=2,threads=1,sockets=1 \
    -m 2097152K \
    -serial mon:stdio -display none -vnc :0 \
    -device virtio-blk-pci,drive=root,id=virtblk0,num-queues=4 \
      -drive file=${QEMU_IMG_FILE},format=qcow2,if=none,id=root \
    -device ahci,id=ahci \
      -cdrom ${QEMU_ISO_FILE} \
    -nic user,model=virtio,id=user.0,hostfwd=tcp::2222-:22
elif [ ${QEMU_ARCH} = "arm64" ]; then
  if [ ! -f nvram.img ]; then
    cp /usr/share/AAVMF/AAVMF_CODE.fd nvram.img
  fi
  screen -d -S qemu-$QEMU_CONFIG_ID -m qemu-system-aarch64 \
    -cpu max -M virt \
    -m 2096 \
    -serial mon:stdio -display none -vnc :0 \
    -pflash /usr/share/AAVMF/AAVMF_CODE.fd \
    -pflash nvram.img \
    -device virtio-blk-device,drive=root \
      -drive if=none,file=${QEMU_IMG_FILE},id=root \
    -drive if=virtio,format=raw,file=${QEMU_ISO_FILE} \
    -nic user,model=virtio,id=user.0,hostfwd=tcp::2222-:22
fi

echo "QEMU machine is booting .."
echo "  console: screen -RDS qemu-$QEMU_CONFIG_ID"
echo "  ssh: ssh -o SendEnv="*" -i /root/qemu/ssh_key localhost -p2222"
