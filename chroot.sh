#!/bin/bash

case $(uname -m) in
  x86_64)
    ARCH=amd64 ;;
  aarch64)
    ARCH=arm64 ;;
  *)
    ARCH=$(uname -m) ;;
esac

if [ ! -f tricorder-fs-${ARCH}.tar ]; then
  echo "Did not find tricorder-fs-${ARCH}.tar, trying to create it with docker export"
  docker compose up -d
  docker export tricorder -o tricorder-fs-${ARCH}.tar
  if [ $? -ne 0 ]; then
    exit 1
  fi
  docker compose down
fi

if [ ! -d tricorder-fs ]; then
  mkdir tricorder-fs
  tar -xf tricorder-fs-${ARCH}.tar -C tricorder-fs
fi

sudo mount -t proc /proc tricorder-fs/proc
sudo mount -t sysfs /sys tricorder-fs/sys
sudo mount -o bind /dev tricorder-fs/dev

sudo chroot tricorder-fs /bin/bash -i

sudo umount tricorder-fs/proc
sudo umount tricorder-fs/sys
sudo umount tricorder-fs/dev
